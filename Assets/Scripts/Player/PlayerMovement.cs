﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;

    public Transform playerTransform;
    public float speed;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(0f, horizontal * Time.deltaTime * speed, 0f);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(0f, horizontal * Time.deltaTime * speed, 0f);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(-vertical * Time.deltaTime * speed, 0f, 0f);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(-vertical * Time.deltaTime * speed, 0f, 0f);
        }
    }
}
