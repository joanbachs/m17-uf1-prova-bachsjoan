﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] enemies = new GameObject[2];

    float spawnRate = 5f;
    float nextSpawn = 0.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        if (Time.time > nextSpawn)
        {
            int enemy = Random.Range(0, 2);
            spawnRate = Random.Range(1, 6);
            nextSpawn = Time.time + spawnRate;
            Instantiate(enemies[enemy], transform.position, Quaternion.identity);
        }
    }
}
