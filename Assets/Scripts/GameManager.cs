﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public GameObject Player;
    public ScriptUI scriptUI;

    private float _score;

    public float Score
    {
        get { return _score; }
    }

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        _score = 0;

        Player = GameObject.Find("Player");
        scriptUI = GameObject.Find("Canvas").GetComponent<ScriptUI>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void IncreaseScore(float increment)
    {
        _score += increment;
    }

}
