﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FRMovement : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;

    [SerializeField] private float speed;
    private Transform FragateTransform;


    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        FragateTransform = GetComponent<Transform>();
        speed = 2;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0f, (Time.deltaTime * speed), 0f);

    }
}
