﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrShoot : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    private float fireRate;


    private void Start()
    {
        fireRate = 1;
    }
    // Update is called once per frame
    void Update()
    {
        if (fireRate <= 0)
        {
            ShootBullet();
            fireRate = 1;
        }
        else
        {
            fireRate -= Time.deltaTime;
        }
        
        
    }

    void ShootBullet()
    {
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }
}
