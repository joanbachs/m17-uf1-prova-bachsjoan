﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFMovement : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;

    [SerializeField] private float speed;
    private Transform AlienTransform;


    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        AlienTransform = GetComponent<Transform>();
        speed = 7;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0f, (Time.deltaTime * speed), 0f);

    }
}
