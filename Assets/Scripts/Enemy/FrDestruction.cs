﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrDestruction : MonoBehaviour
{
    private int lives;
    private void Start()
    {
        lives = 5;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Bullet"))
        {
            lives--;

            if (lives <= 0)
            {
                GameManager.Instance.IncreaseScore(10);
                Destroy(gameObject);
            }
        }
    }
}
